module gitlab.com/eukano/fractal

go 1.17

require (
	github.com/cnkei/gospline v0.0.0-20191204072713-842a72f86331
	github.com/gotk3/gotk3 v0.6.1
	gonum.org/v1/gonum v0.9.3
)
