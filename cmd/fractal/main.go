package main

import (
	"embed"
	"log"
	"math"
	"os"

	"github.com/gotk3/gotk3/cairo"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

//go:embed fractal.glade
var files embed.FS

type App struct {
	*gtk.Application
	Win     *gtk.ApplicationWindow
	Draw    *gtk.DrawingArea
	Actions map[string]*glib.SimpleAction

	scale  float64
	center complex128
	fract  Fractal
}

func main() {
	gtk.Init(&os.Args)

	app := new(App)
	app.fract = Mandlebrot

	var err error
	app.Application, err = gtk.ApplicationNew("com.eukano.fractal", 0)
	if err != nil {
		log.Fatal(err)
	}

	app.Connect("startup", app.startup)
	app.Connect("activate", app.activate)

	os.Exit(app.Run(os.Args))
}

func (app *App) startup() {
	var vars struct {
		MenuBar  *glib.MenuModel
		DrawArea *gtk.DrawingArea
	}
	_, err := LoadFS(files, "fractal.glade", &vars)
	if err != nil {
		log.Fatal(err)
	}

	actions := map[string]interface{}{
		"quit": app.Quit,
	}
	app.Actions = make(map[string]*glib.SimpleAction, len(actions))
	for name, activate := range actions {
		act := glib.SimpleActionNew(name, nil)
		app.Actions[name] = act
		if activate != nil {
			act.Connect("activate", activate)
		}
		app.AddAction(act)
	}

	app.SetAccelsForAction("app.quit", []string{"<Primary>Q"})
	app.SetMenubar(vars.MenuBar)

	app.Win, err = gtk.ApplicationWindowNew(app.Application)
	if err != nil {
		log.Fatal(err)
	}

	app.Win.SetTitle("Fratcals")
	app.Win.SetDefaultSize(800, 500)

	app.Win.Connect("destroy", app.Quit)

	app.Draw = vars.DrawArea
	app.Draw.Connect("draw", app.draw)
	app.Win.Add(vars.DrawArea)
}

func (app *App) activate() {
	app.Win.ShowAll()
}

func (app *App) draw(da *gtk.DrawingArea, cr *cairo.Context) {
	W, H := da.GetAllocatedWidth(), da.GetAllocatedHeight()
	scale := math.Min(float64(W), float64(H)) / 3
	image := Calculate(app.fract, W, H, scale, -0.6)

	for x := 0; x < W; x++ {
		for y := 0; y < H; y++ {
			c := image[x*H+y]
			cr.SetSourceRGB(c[0], c[1], c[2])
			cr.Rectangle(float64(x), float64(y), 1, 1)
			cr.Fill()
		}
	}
}
