package main

import "gonum.org/v1/gonum/interp"

type Gradient [3]interp.FritschButland

var G1 = func() *Gradient {
	g := new(Gradient)
	x := []float64{0, 0.16, 0.42, 0.6425, 0.8575, 1}
	_ = g[0].Fit(x, []float64{0.00, 0.13, 0.93, 1.00, 0.00, 0.00})
	_ = g[1].Fit(x, []float64{0.03, 0.42, 1.00, 0.67, 0.01, 0.03})
	_ = g[2].Fit(x, []float64{0.40, 0.80, 1.00, 0.00, 0.00, 0.40})
	return g
}()

var G1_2048 = G1.Precalculate(2048)

func (g *Gradient) At(i float64) (v [3]float64) {
	v[0] = g[0].Predict(i)
	v[1] = g[1].Predict(i)
	v[2] = g[2].Predict(i)
	return v
}

func (g *Gradient) Precalculate(n int) (v [][3]float64) {
	v = make([][3]float64, n)
	for i := range v {
		v[i] = g.At(float64(i) / float64(n-1))
	}
	return v
}
