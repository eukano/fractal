package main

func sqmag(v complex128) float64 {
	return real(v)*real(v) + imag(v)*imag(v)
}
