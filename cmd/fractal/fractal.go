package main

import (
	"fmt"
	"math"
	"time"
)

type Fractal func(z, c complex128) (v complex128, esc bool)

func Mandlebrot(z, c complex128) (complex128, bool) {
	z = z*z + c
	return z, sqmag(z) > 10
}

func Calculate(f Fractal, W, H int, scale float64, center complex128) (image [][3]float64) {
	const N = 100

	t := time.Now()
	inSet := make([]bool, W*H)
	pixels := make([]float64, W*H)
	for x := 0; x < W; x++ {
		r := float64(W/2-x) / scale

		for y := 0; y < H; y++ {
			i := float64(H/2-y) / scale
			c := complex(r, i) + center

			var it int
			var z complex128
			var esc bool
			for ; it < N && !esc; it++ {
				z, esc = f(z, c)
			}
			if !esc {
				inSet[x*H+y] = true
				continue
			}

			pixels[x*H+y] = (float64(it) + 1 - math.Log2(math.Log(sqmag(z))/2/2)) / N
		}
	}

	image = make([][3]float64, W*H)
	for x := 0; x < W; x++ {
		for y := 0; y < H; y++ {
			if inSet[x*H+y] {
				image[x*H+y] = [3]float64{}
				continue
			}

			v := pixels[x*H+y] * 1.5
			i := int(v * 2047)
			image[x*H+y] = G1_2048[i%2048]
		}
	}

	fmt.Println(time.Since(t))
	return image
}
